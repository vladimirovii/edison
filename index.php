<?
	// include file config
	require_once 'config.php';

	//if not file config.db to install
	if(!file_exists(INCLUDES_DIR . 'config.db.php'))
	{
		header('Location: install.php');
	}
	else
	{
		// input const DB
		require_once INCLUDES_DIR . 'config.db.php';
		// check autorized
		if(!Session::authtrue())
			include_once TPL_DIR . 'index.php';
		else
			include_once TPL_DIR . 'users/index.php';
	}
?>