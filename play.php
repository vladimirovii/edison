<?php
	require_once 'config.php';
	require_once INCLUDES_DIR . 'config.db.php';
	include_once TPL_DIR . 'users/data.php';

	if(!$game->allField($idSession) || $game->win($idSession, $idOpponent))
		echo "<script type='text/javascript'>
				$(document).ready(function(){
					$('#winForm').fadeTo('slow', 1);
				});
			  </script>";
?>
<center>
	<table>
	<?php
		if($move == 1)
			echo "<h2>" . PO_MOVE . "</h2>";
		$field = $game->getField($idSession);
		$field = unserialize($field);
		if(empty($field))
			die(PO_ENDGAME);	
		foreach ($field as $key1 => $value1)  {
			echo "<tr>";
			foreach ($value1 as $key2 => $value2) { 
				echo "<td onclick='location.href=\"?col=". $key1 ."&row=". $key2 ." \" '>";
				if ($field[$key1][$key2] == 1)
					echo "<img src='" . DOMAIN . "/content/template/css/1.png' />";
				if ($field[$key1][$key2] == 2)
					echo "<img src='" . DOMAIN . "/content/template/css/2.png' />";
				echo "</td>";

			}
			echo "</tr>";
		}
	?>
	</table>
</center>