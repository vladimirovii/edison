<?php
	require_once 'config.php';
	require_once INCLUDES_DIR . 'config.db.php';
	include_once TPL_DIR . 'users/data.php';
	if($idGame)
		echo "<script> location.reload(); </script>";
?>
<center>
	<table>
		<h1><?=PO_USERS_ONLINE;?></h1>
		<tr>
			<td><?=PO_USERNAME;?>:</td>
			<td><?=PO_INVITE_TO_GAME;?></td>
			<td><?=PO_STATUS_TO_GAME;?></td>
		</tr>
		<?php
			$query = $online->selectOnline();
			$query->setFetchMode(PDO::FETCH_ASSOC);
			while($row = $query->fetch()){
				if($row['userid'] != $idUser){
		?>
		<tr>
			<td><?=$row['userlogin'];?></td>
			<td>
				<form action="?request=<?=$row['userid'];?>" method="post">
					<input class="button" type="submit" value="<?=PO_INVITE_TO;?>" name="request"></form>
			</td>
			<td>
				<?php if($row['session'] != 0)
							echo PO_GAMED;
						else
							echo PO_FREE;
				?></td>
		</tr>
		<?php
				}
			}
		?>
	</table>
</center>

<?php
	if($online->getReplyOnline($idUser) != 0)
		echo "<script>
					$(document).ready(function(){
					$('#formRequest').fadeTo('fast', 1);
					})();
    		</script>";
?>
