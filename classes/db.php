<?php
/**
* Use the singleton pattern connect to database
*/
class DB
{
    static private $_conn;
    private $db;

    /**
  	 * Create PDO instance representing a connection to database
  	 */
    private function __construct()
    {
    	try
        {
	        $this->db = new PDO("mysql:host=".HOSTDB.";dbname=".DATABASEDB, USERDB, PASSWORDDB);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
        catch (PDOException $e)
		{
            echo "PDO connection error: " . $e->getMessage();
        }
    }

    public function __clone()
    {
        return false;
    }

    public function __wakeup()
    {
        return false;
    }

    public static function getConnection()
    {
    	if (empty(static::$_conn)) {
        	static::$_conn = new DB();
        }
        return static::$_conn;
    }
    /**
     * [DBquery description]
     * @param [type] $sql [description]
     */
    public function DBquery($sql)
    {
    	return $this->db->query($sql);
    }
    /**
     * [DBfetchAssoc description]
     * @param [type] $obj [description]
     */
    public function DBfetchAssoc($obj)
    {
        $obj->setFetchMode(PDO::FETCH_ASSOC);
        return $obj->fetch();
    }
}
?>