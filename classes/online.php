<?php
class Online
{

	private $conn;
	/**
	 * [__construct description] connect to database
	 * @param [type] $_conn [description]
	 */
	public function __construct($_conn){
		$this->conn = $_conn;
	}
	/**
	 * [startSessionOnline description]
	 * @param  [type] $userid    [description]
	 * @param  [type] $userlogin [description]
	 * @return [type]            [description]
	 */
	public function startSessionOnline($userid, $userlogin){
		$this->conn->DBquery("INSERT INTO online (userid, userlogin) VALUES ('$userid', '$userlogin')");
	}
	/**
	 * [endSessionOnline description]
	 * @param  [type] $userid [description]
	 * @return [type]         [description]
	 */
	public function endSessionOnline($userid){
		$this->conn->DBquery("DELETE FROM online WHERE userid='$userid'");
	}
	/**
	 * [selectOnline description]
	 * @return [type] [description]
	 */
	public function selectOnline(){
		$query = $this->conn->DBquery("SELECT userid, userlogin, session FROM online");
		return $query;
	}
	/**
	 * [requestOnline description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function requestOnline($id, $options){
		$this->conn->DBquery("UPDATE online SET session='$id', move=1, options='$options' WHERE userid = '$id'");
	}
	/**
	 * [replyOnline description]
	 * @param  [type] $id     [description]
	 * @param  [type] $idhost [description]
	 * @return [type]         [description]
	 */
	public function replyOnline($id, $idhost, $options){
		$this->conn->DBquery("UPDATE online SET reply='$idhost', session='$idhost', options='$options' WHERE userid = '$id'");
	}
	/**
	 * [getOptions description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getOptions($id){
		$query = $this->conn->DBquery("SELECT options FROM online WHERE userid = '$id'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['options'];
	}
	/**
	 * [getReplyOnline description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getReplyOnline($id){
		$query = $this->conn->DBquery("SELECT reply FROM online WHERE userid = '$id'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['reply'];
	}
	/**
	 * [replyOkOnline description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function replyOkOnline($id){
		$this->conn->DBquery("UPDATE online SET reply=0 WHERE userid = '$id'");
	}
	/**
	 * [replyCancelOnline description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function exitOnline($id){
		$this->conn->DBquery("UPDATE online SET reply=0, session=0, move=0, options=0 WHERE userid = '$id'");
	}
	/**
	 * [setMoveOnline description]
	 * @param [type] $id [description]
	 */
	public function setMoveOnline($id){
		$this->conn->DBquery("UPDATE online SET move=1 WHERE userid = '$id'");
	}
	/**
	 * [getMoveOnline description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getMoveOnline($id){
		$query = $this->conn->DBquery("SELECT move FROM online WHERE userid = '$id'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['move'];
	}
	/**
	 * [removeMoveOnline description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function removeMoveOnline($id){
		$this->conn->DBquery("UPDATE online SET move=0 WHERE userid = '$id'");
	}
	/**
	 * [getIdGameSession description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getIdGameSession($id){
		$query = $this->conn->DBquery("SELECT session FROM online WHERE userid='$id'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['session'];
	}
	/**
	 * [getIdOpponent description]
	 * @param  [type] $idsession [description]
	 * @param  [type] $idmy      [description]
	 * @return [type]            [description]
	 */
	public function getIdOpponent($idsession, $idmy){
		$query = $this->conn->DBquery("SELECT userid FROM online WHERE session='$idsession' AND userid !='$idmy'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['userid'];
	}
	/**
	 * [issetSession description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function issetSession($id){
		$query = $this->conn->DBquery("SELECT session FROM online WHERE userid='$id'");
		$result = $this->conn->DBfetchAssoc($query);
		if($result['session'] != 0)
			return false;
		return true;
	}
}
//SELECT session FROM online WHERE session IN (SELECT session FROM online GROUP BY session HAVING COUNT(*)>1)
?>