<?php
class Register{

	private static $conn;
	private static $session;

	public function __construct($_conn){
		static::$conn = $_conn;
		static::$session = new Session($_conn);
	}
	/**
	 * [userToRegister description] processing data and call finction ErrorgetEmail
	 * @param  [type] array [description]
	 * @return [type]       [description]
	 */
	public function userToRegister($arr){
		$login = Simple::ClearData($arr['login'], 's');
		$email = Simple::ClearData($arr['email'], 's');
		$password = Simple::HashPassword($arr['password']);
		if($login == "" && $email == "")
			return false;
		// check email
		if(!self::ErrorgetEmail($email)){
			die ("User witch this email exec");
		}
		if(!self::newUsers($login, $email, $password))
			return false;
		return true;
	}
	/**
	 * [userToAuthorised description] processing data and call finction getValidateAuthorized
	 * @param  [type] $arr array [description]
	 * @return [type]       [description]
	 */
	public function userToAuthorised($arr){
		$login = Simple::ClearData($arr['login'], 's');
		$password = Simple::HashPassword($arr['password']);
		if(!self::getValidateAuthorized($login, $password))
			return false;
		else
			return true;
	}
	/**
	 * [getValidateAuthorized description] authorised user
	 * @param  [type] $login string    [description]
	 * @param  [type] $password string    [description]
	 * @return [type]           [description]
	 */
	private static function getValidateAuthorized($login, $password){
		$query = static::$conn->DBquery("SELECT login, password FROM users WHERE login='$login'");
		$result = static::$conn->DBfetchAssoc($query);
		if($result['login'] == $login && $result['password'] == $password){
			// session start
			try {
				if(!static::$session->newSessionUsersLogin($login))
					throw new Exception('Errors Session');
				else
					return true;
			} catch (Exception $e) {
				return $e->setMessage();
			}
		}else
			return false;
	}
	/**
	 * [newUsers description] rigester new user
	 * @param  [type] $login string    [description]
	 * @param  [type] $email string    [description]
	 * @param  [type] $password string    [description]
	 * @return [type]           [description]
	 */
	private static function newUsers($login, $email, $password){
		$query = static::$conn->DBquery("INSERT INTO users (login, email, password, role) VALUES ('$login', '$email', '$password', 1)");
		if($query){
			//start session
			try {
				if(!static::$session->newSessionUsersLogin($login))
					throw new Exception('Errors Session');
				else
					return true;
			} catch (Exception $e) {
				return $e->setMessage();
			}
		}else
			return false;
	}
	/**
	 * [ErrorgetEmail description] exec user witch email?
	 * @param [type] $email string [description]
	 */
	private static function ErrorgetEmail($email){
		$query = static::$conn->DBquery("SELECT email FROM users WHERE email='$email'");
		$result = static::$conn->DBfetchAssoc($query);
		if($result['email'] == $email)
			return false;
		else
			return true;
	}
}
?>