<?php
/**
 * class processing game sessions in session table
 */
class GameSessions
{

	private $conn;
	/**
	 * [__construct description] connect to database
	 * @param [type] $_conn [description]
	 */
	public function __construct($_conn){
		$this->conn = $_conn;
	}
	/**
	 * [startGameSession description]
	 * @param  [type] $idsession [description]
	 * @return [type]            [description]
	 */
	public function startGameSession($idsession, $field){
		$this->conn->DBquery("INSERT INTO gamesessions (sessionid, field) VALUES ('$idsession', '$field')");
	}
	/**
	 * [endGameSession description]
	 * @param  [type] $idsession [description]
	 * @return [type]            [description]
	 */
	public function endGameSession($idsession){
		$this->conn->DBquery("DELETE FROM gamesessions WHERE sessionid='$idsession'");
	}
	/**
	 * [getGameSession description]
	 * @param  [type] $idsession [description]
	 * @return [type]            [description]
	 */
	public function getGameSession($idsession){
		$query = $this->conn->DBquery("SELECT sessionid FROM gamesessions WHERE sessionid='$idsession'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['sessionid'];
	}
	/**
	 * [getCols description]
	 * @param  [type] $idsession [description]
	 * @return [type]            [description]
	 */
	public function field($idsession){
		$query = $this->conn->DBquery("SELECT field FROM gamesessions  WHERE sessionid='$idsession'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['field'];
	}
	/**
	 * [updateField description]
	 * @param  [type] $idsession [description]
	 * @param  [type] $field     [description]
	 * @return [type]            [description]
	 */
	public function updateField($idsession, $field){
		$this->conn->DBquery("UPDATE gamesessions SET field='$field' WHERE sessionid='$idsession'");
	}
}
?>