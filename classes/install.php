<?php
class Install
{
	private $conn;

	public function __construct($_conn){
		$this->conn = $_conn;
	}
	/**
	 * [start description] install table and set user Administrator
	 * @param  [type] array  [description]
	 * @return [type]        [description]
	 */
	public function start($admin){
		try{
			// Setup Database
			if(!$this->tables())
				throw new Exception("error created tables");
			// creating admin
			$login = Simple::ClearData($admin[0], 's');
			$email = Simple::ClearData($admin[1], 's');
			$password = $admin[2];
			if(!$this->newAdmin($login, $email, $password))
				throw new Exception("error created admin");
		}catch(Exception $e){
			return $e->getMessage();
		}
	}
	/**
	 * [tables description] insert table to database
	 * @return [type] boolean [description] true if insert tables success
	 */
	public function tables(){
		$this->conn->DBquery('set names utf8');
		$this->conn->DBquery("SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0");
		$this->conn->DBquery("SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0");
		$this->conn->DBquery("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
		$this->conn->DBquery("CREATE SCHEMA IF NOT EXISTS " . DATABASEDB . " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ; USE " . DATABASEDB);

		$users = $this->conn->DBquery(
		"CREATE TABLE users (
		idusers INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		login VARCHAR(30) NOT NULL,
		password VARCHAR(150) NOT NULL,
		email VARCHAR(50),
		role INT(1) NOT NULL
		)");

		$online = $this->conn->DBquery(
		"CREATE TABLE online (
		idonline INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		userid INT(11) NOT NULL,
		userlogin VARCHAR(30) NOT NULL,
		reply INT(11) NOT NULL,
		session INT(11) NOT NULL,
		move INT(1) NOT NULL
		)");

		$session = $this->conn->DBquery(
		"CREATE TABLE gamesessions (
		idsession INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		sessionid INT(11) NOT NULL,
		field VARCHAR(150) NOT NULL
		)");

		$this->conn->DBquery("SET SQL_MODE=@OLD_SQL_MODE");
		$this->conn->DBquery("SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS");
		$this->conn->DBquery("SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS");

		if(!$users) return false;
		if(!$online) return false;
		if(!$session) return false;

		return true;
	}
	/**
	 * [newAdmin description]
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @param  [type] string    [description]
	 * @return [type]           [description] true if insert administrator success
	 */
	private function newAdmin($login, $email, $password){
		$result = $this->conn->DBquery("INSERT INTO users (login, email, password, role) VALUES ('$login', '$email', '$password', '3')");
		if(!$result) return false;
		return true;
	}
}
?>
