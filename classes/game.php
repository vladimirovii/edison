<?php
/**
* class processing start game, finished or restarted
*/
class Game extends GameSessions
{
	private $online;
	/**
	 * [__construct description]
	 * @param [type] $_conn  [description]
	 * @param [type] $online [description]
	 */
	public function __construct($_conn, $online){
		$this->online = $online;
		parent::__construct($_conn);
	}
	/**
	 * [prepare description]
	 * @param  [type] $id          [description]
	 * @param  [type] $fieldWidth  [description]
	 * @param  [type] $fieldHeight [description]
	 * @param  [type] $idUser      [description]
	 * @return [type]              [description]
	 */
	public function prepare($id, $fieldWidth, $fieldHeight, $idUser){
		$options = $fieldWidth . "#" . $fieldHeight;
		$this->online->requestOnline($idUser, $options);
		$this->online->replyOnline($id, $idUser, $options);
		header("Location: index.php");
	}
	/**
	 * [start description]
	 * @param  [type] $idUser     [description]
	 * @param  [type] $idOpponent [description]
	 * @return [type]             [description]
	 */
	public function start($idUser, $idOpponent){
		$this->online->replyOkOnline($idUser);
		$field = explode("#", $this->online->getOptions($idOpponent));
		$width = array_fill(1, $field[0], 0);
		for ($i=1; $i <= (int)$field[1] ; $i++) { 
			$height[$i] = $width;
		}
		$fields = serialize($height);
		$this->startGameSession($idOpponent, $fields);
		header("Location: index.php");
	}
	/**
	 * [getIdGame description]
	 * @param  [type] $idSession [description]
	 * @return [type]            [description]
	 */
	public function getIdGame($idSession){
		return $this->getGameSession($idSession);
	}
	/**
	 * [getField description]
	 * @param  [type] $idSession [description]
	 * @return [type]            [description]
	 */
	public function getField($idSession){
		return $this->field($idSession);
	}
	/**
	 * [move description]
	 * @param  [type] $idcol      [description]
	 * @param  [type] $idrow      [description]
	 * @param  [type] $idUser     [description]
	 * @param  [type] $idSession  [description]
	 * @param  [type] $move       [description]
	 * @param  [type] $idOpponent [description]
	 * @return [type]             [description]
	 */
	public function move($idcol, $idrow, $idUser, $idSession, $move, $idOpponent){
		if($idSession == $idUser)
			$chess = 1;
		else
			$chess = 2;
		if($move == 1){
			$field = $this->getField($idSession);
			$field = unserialize($field);
			foreach ($field as $key1 => $value1)  {
				foreach ($value1 as $key2 => $value2) { 
					if(($key1 == $idcol && $key2 == $idrow) && $field[$key1][$key2] == 0){
						$field[$key1][$key2] = $chess;
						$this->online->removeMoveOnline($idUser);
						$this->online->setMoveOnline($idOpponent);
					}
				}
			}
			$field = serialize($field);
			$this->updateField($idSession, $field);		
		}
		header("Location: index.php");
	}
	/**
	 * [restart description]
	 * @param  [type] $idSession [description]
	 * @return [type]            [description]
	 */
	public function restart($idSession){
		$field = $this->getField($idSession);
		$field = unserialize($field);
		if(empty($field))
			exit();
		foreach ($field as $key1 => $value1)  {
			foreach ($value1 as $key2 => $value2) { 
				$field[$key1][$key2] = 0;
			}
		}
		$field = serialize($field);
		$this->updateField($idSession, $field);
	}
	/**
	 * [gamexit description]
	 * @param  [type] $idUser     [description]
	 * @param  [type] $idOpponent [description]
	 * @param  [type] $idSession  [description]
	 * @return [type]             [description]
	 */
	public function gamexit($idUser, $idOpponent, $idSession){
		$this->endGameSession($idSession);
		$this->online->exitOnline($idOpponent);
		$this->online->exitOnline($idUser);
		header("Location: index.php");
	}
	/**
	 * [allField description]
	 * @param  [type] $idsession [description]
	 * @return [type]            [description]
	 */
	public function allField($idSession){
		$field = $this->getField($idSession);
		$field = unserialize($field);
		if(empty($field))
			exit();
		foreach ($field as $key1 => $value1)  {
			if (in_array(0,  $value1))
				return true;
		}	
	}
	/**
	 * [win description]
	 * @param  [type] $idsession [description]
	 * @return [type]            [description]
	 */
	public function win($idSession, $idOpponent){
		$field = $this->getField($idSession);
		$field = unserialize($field);
		if(empty($field))
			exit();
		$size = explode("#", $this->online->getOptions($idOpponent));
		for ($i=1; $i <= (int)$size[0]; $i++) {
			foreach ($field as $key1 => $value1)  {
				if (($value1[$i] != 0) && ($value1[$i] == $value1[$i + 1]) && ($value1[$i] == $value1[$i + 2]))
					return $value1[$i];
			}
		}
		for ($n=1; $n <= (int)$size[1]; $n++) { 
			$el = array();
			foreach ($field as $key1 => $value1) {
				$el[] = $value1[$n];
			}
			for ($i=0; $i < count($el) ; $i++) { 
				if(($el[$i] != 0) && ($el[$i] == $el[$i + 1]) && ($el[$i] == $el[$i + 2]))
					return $el[$i];
			}
		}
		foreach ($field as $key1 => $value1)  {
			foreach ($value1 as $key2 => $value2) { 
				if($field[$key1][$key2] != 0)
					if(($field[$key1+1][$key2-1] == $field[$key1][$key2]))
						if($field[$key1+2][$key2-2] == $field[$key1][$key2])
							return $field[$key1][$key2];
				if($field[$key1][$key2] != 0)
					if(($field[$key1+1][$key2+1] == $field[$key1][$key2]))
						if($field[$key1+2][$key2+2] == $field[$key1][$key2])
							return $field[$key1][$key2];
			}
		}
	}
}
?>