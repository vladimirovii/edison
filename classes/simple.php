<?
	/**
	 * Simple processing data
	 */
class Simple
{
	/**
	 * [openTextFiles description] function open files
	 * @param  [type] string  [description] file name is get content
	 * @return [type]         [description]
	 */
	public static function openTextFiles($file){
		if(file_get_contents($file)){
			return file_get_contents($file);
		}else
			return null;
	}
	/**
	 * [clearData description] processing all data
	 * @param  [type] $data string [description]
	 * @param  [type] $flag string [description]
	 * @return [type] $data string [description]
	 */
	public static function clearData($data, $flag){
		switch ($flag) {
			case 's':
				$data = trim(strip_tags($data));
				break;
			case 'i':
				$data = (int)$data;
				break;
		}
		return $data;
	}
	/**
	 * [hashPassword description] encrypt password
	 * @param  [type] $password string [description]
	 * @return [type]        [description]
	 */
	public static function hashPassword($password){
		return md5($password);
	}
	/**
	 * [getRole description] get name role
	 * @param  [type] $role string [description]
	 * @return [type] $role string [description] name role
	 */
	static function getRole($role){
		switch ($role) {
			case '3':
				$role = 'Administrator';
				break;
			case '1':
				$role = 'User';
				break;
			default:
				$role = 'Guest';
				break;
		}
		return $role;
	}
}
?>