<?
/**
 * class processing Users acсount
 */
class User
{

	private $conn;
	/**
	 * [__construct description] connect to database
	 * @param [type] $_conn [description]
	 */
	public function __construct($_conn){
		$this->conn = $_conn;
	}
	/**
	 * [getIdUsers description] get id user
	 * @param  [type] $login string [description] user login
	 * @return [type] string [description] id this user
	 */
	public function getIdUsers($login){
		$query = $this->conn->DBquery("SELECT idusers FROM users WHERE login = '$login'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['idusers'];
	}
	/**
	 * [getUser description]
	 * @param  [type] $idusers string [description]
	 * @return [type] array  [description]
	 */
	public function getUser($idusers){
		$id = Simple::clearData($idusers, 'i');
		$query = $this->conn->DBquery("SELECT login FROM users WHERE idusers = '$id'");
		$result = $this->conn->DBfetchAssoc($query);
		return $result['login'];
	}
	/**
	 * [showUsers description] select all users and return this
	 * @return [type] $query [description]
	 */
	public function showUsers(){
		$query = $this->conn->DBquery("SELECT idusers,login,email,role FROM users");
		return $query;
	}
}
?>