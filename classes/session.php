<?
/**
 * Session
 */
class Session
{
	private $user;
	private $online;

	public function __construct($_conn){
		$this->user = new User($_conn);
		$this->online = new Online($_conn);
	}
	/**
	 * [activate description] session start
	 * @return [type] [description]
	 */
	public static function activate(){
		return session_start();
	}
	/**
	 * [newSessionUsersLogin description] check isset session
	 * @param  [type] string   [description]
	 * @return [type] string   [description]
	 */
	public function newSessionUsersLogin($userSession){
		$_SESSION['login'] = $userSession;
		$this->online->startSessionOnline($this->user->getIdUsers($userSession), $userSession);
		return $_SESSION['login'];
	}
	/**
	 * [authtrue description] check user registered or authorised
	 * @return [type] boolean [description]
	 */
	public static function authtrue(){
		if(isset($_SESSION['login']))
			return true;
		else
			return false;
	}
	/**
	 * [logout description] delete session
	 * @return [type] boolean [description]
	 */
	public function logout(){
		if($_SESSION['login']){
			$idUser = $this->user->getIdUsers($_SESSION['login']);
			$idSession = $this->online->getIdGameSession($idUser);
			$idOpponent = $this->online->getIdOpponent($idSession, $idUser);
			//GameEnd::gameExit($idOpponent);
			//GameSessions::endGameSession($idSession);
			$this->online->endSessionOnline($idUser);
			unset($_SESSION['login']);
			return true;
		}else
			return fasle;
	}
}
?>