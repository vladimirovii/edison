<?php 
	// Set Charset
	header("Content-type: text/html; Charset: utf-8");
	// Enable All errors
	error_reporting(E_ALL & ~E_NOTICE);
	// Constants
	define('DOMAIN', "http://" . $_SERVER['HTTP_HOST']);
	define('SITE_DIR', $_SERVER['DOCUMENT_ROOT'] . "/"); 
	define('ENGINE_DIR', SITE_DIR . 'engine/');
	define('CLASSES_DIR', SITE_DIR . 'classes/');
	define('TPL_DIR', SITE_DIR . 'content/template/');
	define('INCLUDES_DIR', SITE_DIR . 'includes/');
	define('LANG_DIR', SITE_DIR . 'lang/');
	
	// autoload classes
	function __autoload($classname){	
		$classname = strtolower($classname);
		require_once CLASSES_DIR . $classname . '.php';
	}

	// session start
	Session::activate();

	// lang setup
	if(isset($_POST['ru']))
		$_SESSION['lang'] = 'ru';
	if(isset($_POST['en']))
		$_SESSION['lang']= 'en';

	if(isset($_SESSION['lang']))
		include LANG_DIR . $_SESSION['lang'] . '.php';
	else
		include LANG_DIR . 'en.php';
?>