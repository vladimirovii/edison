<?php
	// include processing data file
	include_once TPL_DIR . 'users/data.php';
	// include header page user
	include_once TPL_DIR . 'header.php';
	/**
	 *  include home page or game page
	 */
	if(empty($idGame))
		include_once TPL_DIR . 'users/home.php';
	else
		include_once TPL_DIR . 'users/game.php';
	/**
	 *  Logout
	 */
	if(isset($_GET['logout']) == $_SESSION['login']){
		if($session->logout($logout)){
			header('Location: index.php');
		}else{
			die('Error logout');
		}
	}
	/**
	 * input size field game
	 */
	if(isset($_POST['request']))
		echo "<script>	$(document).ready(function(){
		        			$('#gameOptions').fadeTo('fast', 1);
	   					});
			  </script>";
	/**
	 *  request to game
	 */
	if(isset($_POST['gameStart'])){
		$fieldWidth = Simple::clearData($_POST['width'], 'i');
		$fieldHeight = Simple::clearData($_POST['height'], 'i');
		$id = Simple::clearData($_GET['request'], 'i');
		if($online->issetSession($id))
			$game->prepare($id, $fieldWidth, $fieldHeight, $idUser);
	}
	/**
	 * opponent apply game
	 */
	if(isset($_POST['ok'])){
		$game->start($idUser, $idOpponent);
	}
	/**
	 * exit game
	 */
	if(isset($_POST['exit'])){
		$game->gamexit($idUser, $idOpponent, $idSession);
	}
	/**
	 * game restart
	 */
	if(isset($_POST['restart'])){
		$game->restart($idSession);
	}
	/**
	 * player moved
	 */
	if(isset($_GET['col'])){
		$idcol = Simple::clearData($_GET['col'], 'i');
		$idrow = Simple::clearData($_GET['row'], 'i');
		$game->move($idcol, $idrow, $idUser, $idSession, $move, $idOpponent);
	}

	//footer
	include_once TPL_DIR . 'footer.php';
?>