<div class="middle">
	<div class="container" id="tableUsersOnline">
  	<!-- Users online -->
	</div>

	<aside class="right-sidebar">
		<div class="auto">
				<h2><?=PO_ENTER_AS;?><br><?=$_SESSION['login'];?></h2>
				<!-- <a href="?profile" class="btn btn-primary btn-block btn-large">?=PO_PROFILE;?></a>  -->
				<a href="?logout=<?=$_SESSION['login'];?>" class="btn btn-primary btn-block btn-large"><?=PO_OUT;?></a><br/>
		</div>
	</aside><!-- .right-sidebar -->

</div><!-- .middle-->

 <div class="modal fade" id="formRequest" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
      	<div class="modal-body">
      		<h2><?=PO_INVITE;?></h2>
      	</div>
        <div class="modal-footer">
      		<form method="post">
				<button type="submit" name="ok" class="btn btn-success"><?=PO_START;?></button>
				<button type="submit" name="exit" class="btn btn-success"><?=PO_CANCEL;?></button>
			</form>
        </div>
      </div>

    </div>
  </div>

  <div class="modal fade" id="gameOptions" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
      	<div class="modal-body">
      		<h2><?=PO_FIELD;?></h2>
      	</div>
        <div class="modal-footer">
      		<form method="post">
				<p><input type="text" value="3" pattern="[3-9]" name="width"> X
				<input type="text" value="3" pattern="[3-9]" name="height"><br>
				<button type="submit" name="gameStart" class="btn btn-success"><?=PO_START;?></button></p>
			</form>
        </div>
      </div>
    </div>
  </div>

<script>
    function show()
    {
      $.ajax({
      url: "request.php",
      cache: false,
        success: function(html){
           $("#tableUsersOnline").html(html);
        }
      });
    }

    $(document).ready(function(){
        show();
        setInterval('show()',3000);
    });
</script>
