<div id="playGame">
	<!-- Game pad -->
</div><!-- .container-->

<form method="post">
  <p><?=PO_WIN;?></p>
	<button type="submit" name="exit" class="btn btn-primary btn-large"><?=PO_EXIT;?></button>
</form>

<div class="modal fade" id="winForm" role="dialog">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-body">
      			<h2><?=PO_ENDGAME;?></h2>
            <?php if(!$game->allField($idSession)) 
                    echo PO_DEAD;
                  $win = $game->win($idSession, $idOpponent);
                  if($win == 1 && $idUser == $idSession)
                    echo PO_WINTO . $user->getUser($idUser);
                  elseif($win == 2 && $idUser != $idSession)
                    echo PO_WINTO . $user->getUser($idUser);
                  else
                    echo PO_WINTO . $user->getUser($idOpponent);
            ?>
      		</div>
        	<div class="modal-footer">
      			<form method="post">
					<button type="submit" name="restart" class="btn btn-success"><?=PO_RESTART;?></button>
					<button type="submit" name="exit" class="btn btn-success"><?=PO_EXIT;?></button>
				</form>
        	</div>
      	</div>
    </div>
</div>

<script>
    function showgame()
    {
        $.ajax({
        url: "play.php",
        cache: false,
 	    	success: function(html){
    	       $("#playGame").html(html);
        	}
        });
    }

    $(document).ready(function(){
        showgame();
        setInterval('showgame()',2000);
    });
</script>