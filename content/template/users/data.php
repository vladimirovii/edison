<?php
	$user = new User($_conn);
	$online = new Online($_conn);
	$session = new Session($_conn);
	$game = new Game($_conn, $online);

	$idUser = $user->getIdUsers($_SESSION['login']);
	$idSession = $online->getIdGameSession($idUser);
	$idOpponent = $online->getIdOpponent($idSession, $idUser);
	$idGame = $game->getIdGame($idSession);
	$move = $online->getMoveOnline($idUser);
?>