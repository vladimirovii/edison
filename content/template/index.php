<?php
	// include header
	include_once 'header.php';

	// new obj Register
	$register = new Register($_conn);

	try {
		if(isset($_GET['register']))
			include_once TPL_DIR . 'register.php';
		else
			include_once 'home.php';

		// if new user registered
		if(isset($_POST['register'])){
			if(!$register->userToRegister($_POST)) throw new Exception('Error registered');
			else{
				header ('Refresh: 0.5; url=index.php');
				exit();
			}
		}
		// if user authorized
		if(isset($_POST['authorized'])){
			if(!$register->userToAuthorised($_POST)) throw new Exception("User not found");
			else{
				header ('Refresh: 0.5; url=index.php');
				exit();
			}
		}
	}
	catch (Exception $e)
	{
		die($e->getMessage());
	}

	//footer
	include_once 'footer.php';
?>