<div class="reg">
	<form method="post">
		<h1><?=PO_REGISTER;?></h1>
		<center>
		<table>
			<tr><td><?=PO_USERNAME;?>: </td><td><input type="text" name="login" ></td></tr>
			<tr><td>Email: </td><td><input type="text" name="email" ></td></tr>
			<tr><td><?=PO_PASSWORD;?>: </td><td><input type="password" name="password"></td></tr>
			<tr><td><?=PO_PASSWORD_RE;?>: </td><td><input type="password" name="repassword"></td></tr>
		</table>
		</center>
		<p><strong><?=PO_ALL_FIELDS;?></strong></p></br>
		<button type="submit" name="register" class="btn btn-primary btn-large" onclick="return validate(this.form)"><?=PO_REGISTER;?></button>
		<a href="index.php" class="btn btn-primary btn-large"><?=PO_HOME;?></a>
	</form>
</div>