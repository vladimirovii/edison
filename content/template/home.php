<div class="middle">

		<div class="container">
			<main class="content">
					<img width="80%" height="80%" src='<?=DOMAIN;?>/content/template/css/logo.jpg' />
			</main><!-- .content -->
		</div><!-- .container-->

		<aside class="right-sidebar">
			<div class="auto">
				<div class="login">
				<h1><?=PO_LOGIN;?></h1>
			    <form method="post">
			    	<input type="text" name="login" placeholder="<?=PO_USERNAME;?>" required="required" />
			        <input type="password" name="password" placeholder="<?=PO_PASSWORD;?>" required="required" />
			        <button type="submit" name="authorized" class="btn btn-primary btn-block btn-large"><?=PO_ENTER;?></button>
			        <a href="?register" class="btn btn-primary btn-block btn-large"><?=PO_REGISTER;?></a>
			    </form>
			    <form method="post">
			    	<button type="submit" name="ru" class="imglang" ><img src="<?=DOMAIN; ?>/content/template/css/ru.png"></button>
			    	<button type="submit" name="en" class="imglang" ><img src="<?=DOMAIN; ?>/content/template/css/en.png"></button>
			    </form>
				</div>
			</div>
		</aside><!-- .right-sidebar -->

	</div><!-- .middle-->