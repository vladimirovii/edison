function validate(form){
	var login = form.login.value;
	var email = form.email.value;
	var password = form.password.value;
	var repassword = form.repassword.value;

	if(login == ""){
		alert("You not enter login");
		return false;
	}
	var regVL = /^[A-Za-z0-9\-\_]{2,25}$/;
	var result = login.match(regVL);
	if(!result){
		alert("Login include only latin");
		return false;
	}
	if(email == ""){
		alert("You not enter email");
		return false;
	}else{
		var regVE = /^[a-zA-Z0-9\-\_]{2,30}\@[a-zA-Z0-9\-\_]{2,20}\.[a-zA-Z0-9]{2,6}$/;
		result_e = email.match(regVE);
		if(!result_e){
			alert("Incorrect Email!");
			return false;
		}
	}
	if(password == "" || repassword == ""){
		alert("You not enter password");
		return false;
	}
	if(password.length <6 || password.length > 35){
		alert("Length password 6 - 35 char");
		return false;
	}
	if(password != repassword){
		alert("Passwords not identical");
		return false;
	}
	return true;
}