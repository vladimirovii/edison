<?php
 	$errors = "";
 	// post step 3 of install
	if(isset($_POST['third'])){
		require_once INCLUDES_DIR . 'config.db.php';
		$install = new Install($_conn);
		$adminemail = trim(strip_tags($_POST['adminemail']));
		$adminlogin = trim(strip_tags($_POST['adminlogin']));
		$adminpassword = trim(strip_tags($_POST['adminpassword']));
		$adminpassword = md5($adminpassword);
		if($adminemail === "") die ("Enter Email");
		elseif($adminlogin === "" || $adminpassword === "") die ("Enter login and password");
		try {
			$users = array($adminlogin, $adminemail, $adminpassword);
			//Setup
			if(!$install->start($users))
				throw new Exception('Error create DB');
			else{
				Session::logout($_SESSION['login']);
				header('Location: index.php');
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	// post step 2 of install
	if(isset($_POST['second'])){
		$dbname = trim(strip_tags($_POST['dbname']));
		$dbuser = trim(strip_tags($_POST['dbuser']));
		$dbpassword = trim(strip_tags($_POST['dbpassword']));
		$dbhost = trim(strip_tags($_POST['dbserver']));
		if($dbname === "" || $dbuser === "" || $dbhost === "") die ("enter data");
		else{
			//setup config
			$result = "
			<?php
			define ('HOSTDB', '$dbhost');\n
			define ('USERDB', '$dbuser');\n
			define ('PASSWORDDB', '$dbpassword');\n
			define ('DATABASEDB', '$dbname');\n
			\$_conn = DB::getConnection();\n
			?>";
			if(!file_exists(INCLUDES_DIR . 'config.db.php')){
				if(!@file_put_contents(INCLUDES_DIR . 'config.db.php', $result))
					throw new Exception('Error create config.db');
			}
		}
	}
	// post step 1 of install
	if(isset($_POST['system'])){
		$system = isset($_POST['yes']);
		if($system != "ok"){
			 $errors = 1;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Install v1.0</title>
	<link rel="stylesheet" type="text/css" href="<?=DOMAIN; ?>/engine/install/install.css">
	<script type="text/javascript" src="<?=DOMAIN; ?>/content/js/jquery.js"></script>
	<script type="text/javascript" src="<?=DOMAIN; ?>/engine/install/form.js"></script>
</head>
<body>
	<div id="content">
		<div class="header">
			<h2>Install database</h2>
		</div>
	<?php
		if($errors == 1){
			include_once 'errors.php';
			exit;
		}
	?>